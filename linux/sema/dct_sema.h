/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef _DCT_SEMA_H_
#define _DCT_SEMA_H_

#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/of.h>

#define SEMA_LOCKED 1

struct platform_device *dct_devm_sema_get_byphandle(struct device *dev,
						    struct device_node *np,
						    const char *phandle_name);
int dct_sema_lock(struct platform_device *pdev, const uint32_t chan);
int dct_sema_release(struct platform_device *pdev, const uint32_t chan);
int dct_sema_status(struct platform_device *pdev, const uint32_t chan);

#endif /* _DCT_SEMA_H_  */
