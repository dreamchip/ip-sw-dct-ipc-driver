/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/mod_devicetable.h>
#include <linux/of.h>
#include <linux/io.h>

#include <dct_sema.h>

// Number of groups (1..15)
#define NGROUPS_MASK 0x0000FF00U
#define NGROUPS_SHIFT 8U
// Number of semaphores per group (1..32)
#define NSEMA_MASK 0x000000FFU
#define NSEMA_SHIFT 0U

#define SEMA_ID_REG 0x0000
#define SEMA_FEATURE_REG 0x0004

struct sema_driver_data {
	bool initialized;
	struct device *dev;
	void __iomem *base;
	uint32_t nsema;
	uint32_t ngrps;
};

static struct platform_driver sema_driver;
static bool sema_probed(const struct platform_device *pdev);

static struct platform_device *sema_get_byphandle(struct device_node *np,
						  const char *phandle_name)
{
	struct platform_device *pdev;
	struct device_node *sema_np;

	sema_np = of_parse_phandle(np, phandle_name, 0);
	if (!sema_np) {
		return ERR_PTR(-EINVAL);
	}

	if (!of_match_node(sema_driver.driver.of_match_table, sema_np)) {
		of_node_put(sema_np);
		return ERR_PTR(-EINVAL);
	}

	pdev = of_find_device_by_node(sema_np);
	of_node_put(sema_np);
	if (!pdev) {
		return ERR_PTR(-ENODEV);
	}

	// check if initialized
	if (!sema_probed(pdev)) {
		return ERR_PTR(-EAGAIN);
	}

	return pdev;
}

static void sema_put(struct platform_device *pdev)
{
	put_device(&pdev->dev);
}

static void devm_sema_release(struct device *dev, void *res)
{
	struct platform_device **pdev = res;
	sema_put(*pdev);
}

struct platform_device *dct_devm_sema_get_byphandle(struct device *dev,
						    struct device_node *np,
						    const char *phandle_name)
{
	struct platform_device *sema_platform_device;
	struct platform_device **dr;

	dr = devres_alloc(devm_sema_release, sizeof(*dr), GFP_KERNEL);
	if (!dr) {
		return ERR_PTR(-ENOMEM);
	}
	sema_platform_device = sema_get_byphandle(np, phandle_name);
	if (!IS_ERR(sema_platform_device)) {
		*dr = sema_platform_device;
		devres_add(dev, dr);
	} else {
		devres_free(dr);
	}
	return sema_platform_device;
}
EXPORT_SYMBOL(dct_devm_sema_get_byphandle);

static uint32_t sema_get_groups(struct platform_device *pdev)
{
	struct sema_driver_data *driver_data = platform_get_drvdata(pdev);
	return ((readl(driver_data->base + SEMA_FEATURE_REG) & NGROUPS_MASK) >>
		NGROUPS_SHIFT);
}

static uint32_t sema_get_nsema(struct platform_device *pdev)
{
	struct sema_driver_data *driver_data = platform_get_drvdata(pdev);
	return ((readl(driver_data->base + SEMA_FEATURE_REG) & NSEMA_MASK) >>
		NSEMA_SHIFT);
}

int dct_sema_lock(struct platform_device *pdev, const uint32_t chan)
{
	struct sema_driver_data *driver_data;
	void __iomem *sema_addr;
	uint32_t grp, sema;

	if (WARN_ON(!pdev))
		return -EINVAL;

	driver_data = platform_get_drvdata(pdev);

	grp = chan / driver_data->nsema;
	sema = chan % driver_data->nsema;

	if (grp > driver_data->ngrps)
		return -EINVAL;

	sema_addr = driver_data->base + 0x1000 + (0x1000 * grp) + 4 * sema;

	return readl(sema_addr) ? -EBUSY : 0;
}
EXPORT_SYMBOL(dct_sema_lock);

int dct_sema_release(struct platform_device *pdev, const uint32_t chan)
{
	struct sema_driver_data *driver_data;
	void __iomem *sema_addr;
	uint32_t grp, sema;

	if (WARN_ON(!pdev))
		return -EINVAL;

	driver_data = platform_get_drvdata(pdev);

	grp = chan / driver_data->nsema;
	sema = chan % driver_data->nsema;

	if (grp > driver_data->ngrps)
		return -EINVAL;

	sema_addr = driver_data->base + 0x1000 + (0x1000 * grp) + 4 * sema;

	writel(1, sema_addr);

	return 0;
}
EXPORT_SYMBOL(dct_sema_release);

int dct_sema_status(struct platform_device *pdev, const uint32_t chan)
{
	struct sema_driver_data *driver_data;
	void __iomem *sema_addr;
	uint32_t grp, sema;

	if (WARN_ON(!pdev))
		return -EINVAL;

	driver_data = platform_get_drvdata(pdev);

	grp = chan / driver_data->nsema;
	sema = chan % driver_data->nsema;

	if (grp > driver_data->ngrps)
		return -EINVAL;

	sema_addr = driver_data->base + 0x1080 + (0x1000 * grp) + 4 * sema;

	return readl(sema_addr);
}
EXPORT_SYMBOL(dct_sema_status);

static int sema_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	// struct device_node *np = dev->of_node;
	struct sema_driver_data *driver_data;
	struct resource *res;

	dev_info(dev, "probing DCT semaphores ...\n");

	driver_data = devm_kzalloc(dev, sizeof(*driver_data), GFP_KERNEL);
	if (!driver_data) {
		dev_err(dev, "devm_kzalloc for driver_data failed\n");
		return -ENOMEM;
	}
	driver_data->dev = dev;
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(dev, "platform_get_resource failed\n");
		return -ENODEV;
	}

	driver_data->base = devm_ioremap_resource(dev, res);
	if (IS_ERR(driver_data->base)) {
		dev_err(dev, "devm_ioremap_resource %llu %llu failed\n",
			res->start, res->end);
		return PTR_ERR(driver_data->base);
	}

	dev_info(dev, "DCT sema id 0x%08x\n",
		 readl(driver_data->base + SEMA_ID_REG));

	platform_set_drvdata(pdev, driver_data);

	driver_data->nsema = sema_get_nsema(pdev);
	driver_data->ngrps = sema_get_groups(pdev);

	driver_data->initialized = true;

	dev_info(dev, "DCT semaphores initialized\n");

	return 0;
}

static bool sema_probed(const struct platform_device *pdev)
{
	struct sema_driver_data *driver_data;

	driver_data = platform_get_drvdata(pdev);
	return ((NULL != driver_data) && (NULL != pdev->dev.driver) &&
		(driver_data->dev == &pdev->dev) && driver_data->initialized);
}

static int sema_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id sema_match[] = {
	{ .compatible = "dct,dct_sema", .data = 0 },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, sema_match);

static struct platform_driver sema_driver = {
	.driver = {
		.name = "dct_sema",
		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
		.of_match_table = sema_match,
	},
	.probe = sema_probe,
	.remove = sema_remove,
};
module_platform_driver(sema_driver);

MODULE_DESCRIPTION("DCT semaphore driver");
MODULE_LICENSE("GPL");
