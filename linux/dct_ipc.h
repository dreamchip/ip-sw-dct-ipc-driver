/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef _DCT_IPC_H_
#define _DCT_IPC_H_

#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/of.h>

#define IPC_DATA_LEN 116
#define IPC_HDR_LEN sizeof(struct ipc_header)
#define IPC_MSG_LEN (IPC_HDR_LEN + IPC_DATA_LEN)

#define IPC_CMD_ACK 0x80000000

// don't change the order, the other IPC nodes will rely on this given order
enum IPC_NODE { IPC_FSP, IPC_APU, IPC_NPU, IPC_NNA, IPC_MAX_NODES };

struct ipc_header {
	/** payload size */
	uint32_t len;
	/** command */
	uint32_t cmd;
	/** error code */
	uint32_t error;
};

typedef int (*ipc_irq_handler_t)(int irq, void *ctx);

struct platform_device *dct_devm_ipc_get_byphandle(struct device *dev,
						   struct device_node *np,
						   const char *phandle_name);
int dct_ipc_send(struct platform_device *pdev, enum IPC_NODE dst,
		 const uint32_t chan, struct ipc_header *tx);
/* same as above but wait up to timeout_ms if the IPC resource is busy
 * note: the timeout_ms parameter is ignored when called from an interrupt context */
int dct_ipc_send_timeout(struct platform_device *pdev, enum IPC_NODE dst,
		 const uint32_t chan, struct ipc_header *tx, uint32_t timeout_ms);
struct ipc_header *dct_ipc_read(struct platform_device *pdev, enum IPC_NODE src,
				const uint32_t chan);
int dct_ipc_send_receive(struct platform_device *pdev, enum IPC_NODE dst,
			 const uint32_t chan, struct ipc_header *tx,
			 struct ipc_header *rx, const uint32_t max_rx_data_len);
/* same as above but wait up to timeout_ms if the IPC resource is busy
 * note: the timeout_ms parameter is ignored when called from an interrupt context */
int dct_ipc_send_receive_timeout(struct platform_device *pdev, enum IPC_NODE dst,
			 const uint32_t chan, struct ipc_header *tx,
			 struct ipc_header *rx, const uint32_t max_rx_data_len,
			 uint32_t timeout_ms);
int dct_ipc_register_interrupt(const struct platform_device *const pdev,
			       enum IPC_NODE src, const uint32_t chan,
			       ipc_irq_handler_t irq_handler,
			       void *irq_context);

#endif /* _DCT_IPC_H_ */
