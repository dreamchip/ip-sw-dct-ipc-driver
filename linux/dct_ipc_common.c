/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/mod_devicetable.h>
#include <linux/of.h>
#include <linux/io.h>
#include <linux/delay.h>

#include <dct_doorbell.h>
#include <dct_sema.h>
#include <dct_ipc.h>
#include <linux/of_address.h>

struct ipc_db_cfg {
	struct platform_device *pdev;
};

struct ipc_sema_cfg {
	struct platform_device *pdev;
	uint32_t nsaid;
};

struct ipc_driver_data {
	bool initialized;
	struct device *dev;
	void __iomem *ipc_ram;
	size_t ipc_len;
	struct ipc_db_cfg db_cfgs[IPC_MAX_NODES];
	struct ipc_sema_cfg sema_cfgs[IPC_MAX_NODES];
};

static struct platform_driver ipc_driver;
static bool ipc_probed(const struct platform_device *pdev);

static struct platform_device *ipc_get_byphandle(struct device_node *np,
						 const char *phandle_name)
{
	struct platform_device *pdev;
	struct device_node *ipc_np;

	ipc_np = of_parse_phandle(np, phandle_name, 0);
	if (!ipc_np)
		return ERR_PTR(-EINVAL);

	if (!of_match_node(ipc_driver.driver.of_match_table, ipc_np)) {
		of_node_put(ipc_np);
		return ERR_PTR(-EINVAL);
	}

	pdev = of_find_device_by_node(ipc_np);
	of_node_put(ipc_np);
	if (!pdev)
		return ERR_PTR(-ENODEV);

	// check if initialized
	if (!ipc_probed(pdev))
		return ERR_PTR(-EAGAIN);

	return pdev;
}

static void ipc_put(struct platform_device *pdev)
{
	put_device(&pdev->dev);
}

static void devm_ipc_release(struct device *dev, void *res)
{
	struct platform_device **pdev = res;
	ipc_put(*pdev);
}

struct platform_device *dct_devm_ipc_get_byphandle(struct device *dev,
						   struct device_node *np,
						   const char *phandle_name)
{
	struct platform_device *ipc_platform_device;
	struct platform_device **dr;

	dr = devres_alloc(devm_ipc_release, sizeof(*dr), GFP_KERNEL);
	if (!dr)
		return ERR_PTR(-ENOMEM);

	ipc_platform_device = ipc_get_byphandle(np, phandle_name);
	if (!IS_ERR(ipc_platform_device)) {
		*dr = ipc_platform_device;
		devres_add(dev, dr);
	} else {
		devres_free(dr);
	}
	return ipc_platform_device;
}
EXPORT_SYMBOL(dct_devm_ipc_get_byphandle);

int dct_ipc_send(struct platform_device *pdev, enum IPC_NODE dst,
		 const uint32_t chan, struct ipc_header *tx)
{
	struct ipc_driver_data *driver_data = platform_get_drvdata(pdev);

	int ret;
	struct ipc_header *remote;
	struct ipc_sema_cfg *sema_cfg;
	struct ipc_db_cfg *db_cfg;
	uint32_t cmd, sts, err, sema_chan;

	if (tx == NULL || tx->len > IPC_DATA_LEN || dst >= IPC_MAX_NODES ||
	    NULL == pdev)
		return -EINVAL;

	if (chan >= 32)
		return -EINVAL;

	sema_cfg = &(driver_data->sema_cfgs[dst]);
	db_cfg = &(driver_data->db_cfgs[dst]);
	sema_chan = dst * 32 + chan;

	// dev_dbg(&pdev->dev, "dct_ipc_send: dst %d, chan %d, sema chan %d\n", dst, chan, sema_chan);

	if (!db_cfg || !db_cfg->pdev)
		return -EINVAL;

	if (!sema_cfg || !sema_cfg->pdev)
		return -EINVAL;

	sts = dct_sema_status(sema_cfg->pdev, sema_chan);
	if (sts & SEMA_LOCKED)
		return -EBUSY;

	ret = dct_sema_lock(sema_cfg->pdev, sema_chan);
	if (ret < 0)
		return ret;

	sts = dct_sema_status(sema_cfg->pdev, sema_chan);
	if (!(sts & SEMA_LOCKED)) {
		pr_err("%s:%u unexpected error: sema_lock failed.\n", __func__, __LINE__);
		return -EBUSY;
	}

	remote = (struct ipc_header *)(driver_data->ipc_ram +
				       sema_chan * IPC_MSG_LEN);

	memcpy_toio(remote, tx, sizeof(struct ipc_header) + tx->len);
	writel(0, &remote->error);

	ret = dct_db_ring(db_cfg->pdev, chan);

	cmd = readl(&remote->cmd);
	err = readl(&remote->error);

	ret = dct_sema_release(sema_cfg->pdev, sema_chan);
	if (ret < 0)
		return ret;

	if (!(cmd & IPC_CMD_ACK))
		ret = -1;

	return err ? err : ret;
}
EXPORT_SYMBOL(dct_ipc_send);

int dct_ipc_send_timeout(struct platform_device *pdev, enum IPC_NODE dst,
		 const uint32_t chan, struct ipc_header *tx, uint32_t timeout_ms)
{
	int rc = 0;

	if (in_interrupt() && (timeout_ms > 0)) {
		pr_warn("ignoring timeout parameter in interrupt context");
		timeout_ms = 0;
	}

	do {
		rc = dct_ipc_send(pdev, dst, chan, tx);
		if ((rc == 0) || (rc != -EBUSY) || (timeout_ms == 0)) {
			break;
		}
		pr_info("%s: waiting for IPC %u:%u:%x (%u)\n", __func__, dst, chan, tx->cmd, timeout_ms);
		msleep(1);
	} while (timeout_ms--);

	return rc;
}
EXPORT_SYMBOL(dct_ipc_send_timeout);

struct ipc_header *dct_ipc_read(struct platform_device *pdev, enum IPC_NODE src,
				const uint32_t chan)
{
	struct ipc_driver_data *driver_data = platform_get_drvdata(pdev);
	struct ipc_header *hdr;
	uint32_t sema_chan;

	if (src >= IPC_MAX_NODES)
		return NULL;

	if (chan >= 32)
		return NULL;

	sema_chan = src * 32 + chan;

	hdr = (struct ipc_header *)(driver_data->ipc_ram +
				    sema_chan * IPC_MSG_LEN);

	return hdr;
}
EXPORT_SYMBOL(dct_ipc_read);

int dct_ipc_send_receive(struct platform_device *pdev, enum IPC_NODE dst,
			 const uint32_t chan, struct ipc_header *tx,
			 struct ipc_header *rx, const uint32_t max_rx_data_len)
{
	struct ipc_driver_data *driver_data = platform_get_drvdata(pdev);
	int ret;
	struct ipc_header __iomem *remote;
	struct ipc_sema_cfg *sema_cfg;
	struct ipc_db_cfg *db_cfg;
	uint32_t cmd, sts, err, sema_chan;

	if (tx == NULL || tx->len > IPC_DATA_LEN || dst >= IPC_MAX_NODES ||
	    max_rx_data_len > IPC_DATA_LEN || NULL == pdev) {
		return -EINVAL;
	}

	sema_cfg = &(driver_data->sema_cfgs[dst]);
	db_cfg = &(driver_data->db_cfgs[dst]);

	sema_chan = dst * 32 + chan;

	if (!db_cfg->pdev)
		return -EINVAL;

	sts = dct_sema_status(sema_cfg->pdev, sema_chan);
	if (sts & SEMA_LOCKED)
		return -EBUSY;

	ret = dct_sema_lock(sema_cfg->pdev, sema_chan);
	if (ret < 0)
		return ret;

	sts = dct_sema_status(sema_cfg->pdev, sema_chan);
	if (!(sts & SEMA_LOCKED)) {
		pr_err("%s:%u unexpected error: sema_lock failed.\n", __func__, __LINE__);
		return -EBUSY;
	}

	remote = (struct ipc_header __iomem *)(driver_data->ipc_ram +
					       dst * IPC_MSG_LEN);
	memcpy_toio(remote, tx, sizeof(struct ipc_header) + tx->len);
	remote->error = 0;

	dct_db_ring(db_cfg->pdev, chan);

	cmd = remote->cmd;
	err = remote->error;
	if ((cmd & IPC_CMD_ACK) && rx) {
		uint32_t rx_len = remote->len;
		if (err) {
			rx_len = 0;
		} else if (remote->len > max_rx_data_len) {
			rx_len = max_rx_data_len;
		}
		memcpy_fromio(rx, remote, sizeof(struct ipc_header) + rx_len);
		rx->len = rx_len;
	}

	ret = dct_sema_release(sema_cfg->pdev, sema_chan);
	if (ret < 0)
		return ret;

	if (!(cmd & IPC_CMD_ACK))
		ret = -1;

	return err ? err : ret;
}
EXPORT_SYMBOL(dct_ipc_send_receive);

int dct_ipc_send_receive_timeout(struct platform_device *pdev, enum IPC_NODE dst,
			 const uint32_t chan, struct ipc_header *tx,
			 struct ipc_header *rx, const uint32_t max_rx_data_len,
			 uint32_t timeout_ms)
{
	int rc = 0;

	if (in_interrupt() && (timeout_ms > 0)) {
		pr_warn("ignoring timeout parameter in interrupt context");
		timeout_ms = 0;
	}

	do {
		rc = dct_ipc_send_receive(pdev, dst, chan, tx, rx, max_rx_data_len);
		if ((rc == 0) || (rc != -EBUSY) || (timeout_ms == 0)) {
			break;
		}
		pr_info("%s: waiting for IPC %u:%u:%x (%u)\n", __func__, dst, chan, tx->cmd, timeout_ms);
		msleep(1);
	} while (timeout_ms--);

	return rc;
}
EXPORT_SYMBOL(dct_ipc_send_receive_timeout);

int dct_ipc_register_interrupt(const struct platform_device *const pdev,
			       enum IPC_NODE src, const uint32_t chan,
			       ipc_irq_handler_t irq_handler, void *irq_context)
{
	struct ipc_driver_data *driver_data = platform_get_drvdata(pdev);
	struct ipc_db_cfg *db_cfg;

	db_cfg = &(driver_data->db_cfgs[src]);

	return dct_db_register_interrupt(db_cfg->pdev, chan, irq_handler,
					 irq_context);
}
EXPORT_SYMBOL(dct_ipc_register_interrupt);

static int ipc_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct ipc_driver_data *driver_data;
	struct resource *res;
	struct platform_device *db_pdevs[IPC_MAX_NODES];
	struct platform_device *sm_pdevs[IPC_MAX_NODES];
	int i;

	const char *db_names[IPC_MAX_NODES] = {
		[IPC_FSP] = "dct-db-fsp",
		[IPC_NPU] = "dct-db-npu",
		[IPC_NNA] = "dct-db-nna",
		[IPC_APU] = "dct-db-apu",
	};

	for (i = 0; i < IPC_MAX_NODES; i++) {
		dev_info(dev, "[%d]: probing %s ...\n", i, db_names[i]);

		db_pdevs[i] = dct_devm_db_get_byphandle(dev, np, db_names[i]);
		if (!IS_ERR(db_pdevs[i]))
			continue;

		if (PTR_ERR(db_pdevs[i]) == -EAGAIN) {
			dev_info(dev, "waiting for %s ...\n", db_names[i]);
			return -EPROBE_DEFER;
		}

		dev_warn(dev,
			 "dct_devm_db_get_byphandle %d failed, ret = %ld\n", i,
			 PTR_ERR(db_pdevs[i]));
		db_pdevs[i] = NULL;
	}

	sm_pdevs[IPC_FSP] = dct_devm_sema_get_byphandle(dev, np, "dct-sem");
	if (IS_ERR(sm_pdevs[IPC_FSP])) {
		if (PTR_ERR(sm_pdevs[IPC_FSP]) == -EAGAIN) {
			dev_info(dev, "waiting for dct-sem ...\n");
			return -EPROBE_DEFER;
		} else {
			dev_err(dev,
				"dct_devm_sema_get_byphandle FSP failed, ret = %ld\n",
				PTR_ERR(sm_pdevs[IPC_FSP]));
			return PTR_ERR(sm_pdevs[IPC_FSP]);
		}
	}
	for (i = IPC_FSP + 1; i < IPC_MAX_NODES; i++) {
		if (!db_pdevs[i])
			continue;
		sm_pdevs[i] = sm_pdevs[IPC_FSP];
	}

	driver_data = devm_kzalloc(dev, sizeof(*driver_data), GFP_KERNEL);
	if (!driver_data) {
		dev_err(dev, "devm_kzalloc for driver_data failed\n");
		return -ENOMEM;
	}
	driver_data->dev = dev;

	driver_data->db_cfgs[IPC_FSP].pdev = db_pdevs[IPC_FSP];
	driver_data->db_cfgs[IPC_APU].pdev = db_pdevs[IPC_APU];
	driver_data->db_cfgs[IPC_NNA].pdev = db_pdevs[IPC_NNA];
	driver_data->db_cfgs[IPC_NPU].pdev = db_pdevs[IPC_NPU];

	driver_data->sema_cfgs[IPC_FSP].pdev = sm_pdevs[IPC_FSP];
	driver_data->sema_cfgs[IPC_FSP].nsaid = 0x00;

	driver_data->sema_cfgs[IPC_APU].pdev = sm_pdevs[IPC_APU];
	driver_data->sema_cfgs[IPC_APU].nsaid = 0x08;

	driver_data->sema_cfgs[IPC_NNA].pdev = sm_pdevs[IPC_NNA];
	driver_data->sema_cfgs[IPC_NNA].nsaid = 0x00;

	driver_data->sema_cfgs[IPC_NPU].pdev = sm_pdevs[IPC_NPU];
	driver_data->sema_cfgs[IPC_NPU].nsaid = 0x00;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(dev, "platform_get_resource failed\n");
		return -ENODEV;
	}

	driver_data->ipc_ram = devm_ioremap_resource(dev, res);
	if (IS_ERR(driver_data->ipc_ram)) {
		dev_err(dev, "devm_ioremap_resource %llx %llx failed\n",
			res->start, res->end);
		return PTR_ERR(driver_data->ipc_ram);
	}
	driver_data->ipc_len = res->end - res->start;

	dev_info(dev, "devm_ioremap_resource %llx %llx\n", res->start,
		 res->end);

	driver_data->initialized = true;
	platform_set_drvdata(pdev, driver_data);

	dev_info(dev, "DCT IPC initialized\n");

	return 0;
}

static bool ipc_probed(const struct platform_device *pdev)
{
	struct ipc_driver_data *driver_data;

	driver_data = platform_get_drvdata(pdev);
	return ((NULL != driver_data) && (NULL != pdev->dev.driver) &&
		(driver_data->dev == &pdev->dev) && driver_data->initialized);
}

static int ipc_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id ipc_match[] = { { .compatible = "dct,dct_ipc",
						   .data = 0 },
						 { /* sentinel */ } };
MODULE_DEVICE_TABLE(of, ipc_match);

static struct platform_driver ipc_driver = {
	.driver = {
		.name = "dct_ipc",
		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
		.of_match_table = ipc_match,
	},
	.probe = ipc_probe,
	.remove = ipc_remove,
};
module_platform_driver(ipc_driver);

MODULE_DESCRIPTION("DCT Doorbell Driver");
MODULE_LICENSE("GPL");
