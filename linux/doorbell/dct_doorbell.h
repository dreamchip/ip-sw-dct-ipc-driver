/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef _DCT_DOORBELL_H_
#define _DCT_DOORBELL_H_

#include <linux/types.h>
#include <linux/platform_device.h>
#include <linux/device.h>
#include <linux/of.h>

typedef int (*db_irq_handler_t)(int chan, void *ctx);

struct platform_device *dct_devm_db_get_byphandle(struct device *dev,
						  struct device_node *np,
						  const char *phandle_name);
int dct_db_ring(const struct platform_device *const pdev, const uint32_t chan);
int dct_db_register_interrupt(const struct platform_device *const pdev,
			      const uint32_t chan, db_irq_handler_t irq_handler,
			      void *irq_context);

#endif /* _DCT_DOORBELL_H_ */
