/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#undef DEBUG

#include <linux/interrupt.h>
#include <linux/io.h>
#include <linux/irqreturn.h>
#include <linux/mod_devicetable.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_irq.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>

#include <dct_doorbell.h>

#define DB_ID_REG 0x0000
#define DB_FEATURE_REG 0x0004

#define DB_IRQCLRID 0x004
#define DB_IRQSETVEC 0x010
#define DB_IRQSTATVEC 0x018

#define NGROUPS_MASK 0x0000FF00U
#define NGROUPS_SHIFT 8U

#define DB_RING_TIMEOUT 1000

// Number of doorbell groups (1..15)
// Number of interrupts per doorbell group (1..32)
#define NIRQS_MASK 0x000000FFU
#define NIRQS_SHIFT 0U

#define DB_MAX_CHAN 32

struct db_driver_data {
	bool initialized;
	struct device *dev;
	void __iomem *base;
	uint32_t nirqs;
	uint32_t ngrps;
	uint32_t irq_base;
	db_irq_handler_t irq_handler[DB_MAX_CHAN];
	void *irq_context[DB_MAX_CHAN];
};

static struct platform_driver db_driver;
static bool db_probed(const struct platform_device *pdev);

static struct platform_device *db_get_byphandle(struct device_node *np,
						const char *phandle_name)
{
	struct platform_device *pdev;
	struct device_node *db_np;

	db_np = of_parse_phandle(np, phandle_name, 0);
	if (!db_np) {
		return ERR_PTR(-EINVAL);
	}

	if (!of_match_node(db_driver.driver.of_match_table, db_np)) {
		of_node_put(db_np);
		return ERR_PTR(-EINVAL);
	}

	pdev = of_find_device_by_node(db_np);
	of_node_put(db_np);
	if (!pdev) {
		return ERR_PTR(-ENODEV);
	}
	// check if initialized
	if (!db_probed(pdev)) {
		return ERR_PTR(-EAGAIN);
	}

	return pdev;
}

static void db_put(struct platform_device *pdev)
{
	put_device(&pdev->dev);
}

static void devm_db_release(struct device *dev, void *res)
{
	struct platform_device **pdev = res;
	db_put(*pdev);
}

struct platform_device *dct_devm_db_get_byphandle(struct device *dev,
						  struct device_node *np,
						  const char *phandle_name)
{
	struct platform_device *db_platform_device;
	struct platform_device **dr;

	dr = devres_alloc(devm_db_release, sizeof(*dr), GFP_KERNEL);
	if (!dr) {
		return ERR_PTR(-ENOMEM);
	}
	db_platform_device = db_get_byphandle(np, phandle_name);
	if (!IS_ERR(db_platform_device)) {
		*dr = db_platform_device;
		devres_add(dev, dr);
	} else {
		devres_free(dr);
	}
	return db_platform_device;
}
EXPORT_SYMBOL(dct_devm_db_get_byphandle);

static uint32_t db_get_groups(const struct platform_device *const pdev)
{
	struct db_driver_data *driver_data = platform_get_drvdata(pdev);
	return ((readl(driver_data->base + DB_FEATURE_REG) & NGROUPS_MASK) >>
		NGROUPS_SHIFT);
}

static uint32_t db_get_nirqs(const struct platform_device *const pdev)
{
	struct db_driver_data *driver_data = platform_get_drvdata(pdev);
	return ((readl(driver_data->base + DB_FEATURE_REG) & NIRQS_MASK) >>
		NIRQS_SHIFT);
}

int dct_db_ring(const struct platform_device *const pdev, const uint32_t chan)
{
	struct db_driver_data *driver_data = platform_get_drvdata(pdev);
	void __iomem *grp_addr;
	uint32_t grp, nirq;

	if (chan >= DB_MAX_CHAN)
		return -1;

	grp = chan / driver_data->nirqs;
	nirq = chan % driver_data->nirqs;

	if (grp > driver_data->ngrps)
		return -1;

	grp_addr = driver_data->base + 0x1000 + (0x1000 * grp);

	writel(BIT(nirq), grp_addr + DB_IRQSETVEC);

	while (readl(grp_addr + DB_IRQSTATVEC) & BIT(nirq)) {
		;
	}

	return 0;
}
EXPORT_SYMBOL(dct_db_ring);

int dct_db_register_interrupt(const struct platform_device *const pdev,
			      const uint32_t chan, db_irq_handler_t irq_handler,
			      void *irq_context)
{
	struct db_driver_data *driver_data = platform_get_drvdata(pdev);

	if (chan >= DB_MAX_CHAN)
		return -1;

	driver_data->irq_handler[chan] = irq_handler;
	driver_data->irq_context[chan] = irq_context;

	return 0;
}
EXPORT_SYMBOL(dct_db_register_interrupt);

static irqreturn_t db_irq_handler(int irq, void *data)
{
	struct db_driver_data *driver_data = (struct db_driver_data *)data;
	struct device *dev = driver_data->dev;
	unsigned long hwirq = irq_get_irq_data(irq)->hwirq;
	uint32_t chan = hwirq - driver_data->irq_base;
	uint32_t grp = chan / driver_data->nirqs;
	uint32_t id = chan % driver_data->nirqs;
	uintptr_t grp_addr = 0x1000 + (0x1000 * grp);
	db_irq_handler_t cb;
	void *ctx;

	if (chan >= DB_MAX_CHAN || grp > driver_data->ngrps)
		return -1;

	cb = driver_data->irq_handler[chan];
	ctx = driver_data->irq_context[chan];

	dev_dbg(dev, "irq %d, hwirq %lu, chan %d, grp %d, id %d\n", irq, hwirq,
		 chan, grp, id);

	if (cb)
		cb(chan, ctx);

	writel(id, driver_data->base + grp_addr + DB_IRQCLRID);

	return IRQ_HANDLED;
}

static int db_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	// struct device_node *np = dev->of_node;
	struct db_driver_data *driver_data;
	struct resource *res;
	int ret, irq, k;

	dev_info(dev, "probing DCT doorbells ...\n");

	driver_data = devm_kzalloc(dev, sizeof(*driver_data), GFP_KERNEL);
	if (!driver_data) {
		dev_err(dev, "devm_kzalloc for driver_data failed\n");
		return -ENOMEM;
	}
	driver_data->dev = dev;
	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(dev, "platform_get_resource failed\n");
		return -ENODEV;
	}

	dev_dbg(dev, "devm_ioremap_resource %llx %llx\n", res->start, res->end);

	driver_data->base = devm_ioremap_resource(dev, res);
	if (IS_ERR(driver_data->base)) {
		dev_err(dev, "devm_ioremap_resource %llu %llu failed\n",
			res->start, res->end);
		return PTR_ERR(driver_data->base);
	}

	dev_dbg(dev, "DCT doorbells id 0x%08x\n",
		readl(driver_data->base + DB_ID_REG));

	platform_set_drvdata(pdev, driver_data);

	driver_data->nirqs = db_get_nirqs(pdev);
	driver_data->ngrps = db_get_groups(pdev);

	driver_data->irq_base = 32; // Zukimo APU base
	device_property_read_u32(dev, "interrupt-base", &driver_data->irq_base);

	driver_data->irq_base += 32; // GIC-SPI offset

	for (k = 0; k < platform_irq_count(pdev); k++) {
		irq = platform_get_irq(pdev, k);
		if (irq < 0)
			continue;

		ret = devm_request_irq(dev, irq, db_irq_handler, 0,
				       dev_name(dev), driver_data);
		if (ret < 0)
			dev_err(dev,
				"failed to request doorbell IRQ %d, ret %d\n",
				k, ret);
	}

	driver_data->initialized = true;

	dev_dbg(dev, "DCT doorbells initialized\n");

	return 0;
}

static bool db_probed(const struct platform_device *pdev)
{
	struct db_driver_data *driver_data;

	driver_data = platform_get_drvdata(pdev);
	return ((NULL != driver_data) && (NULL != pdev->dev.driver) &&
		(driver_data->dev == &pdev->dev) && driver_data->initialized);
}

static int db_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id db_match[] = { { .compatible = "dct,dct_db",
						  .data = 0 },
						{ /* sentinel */ } };
MODULE_DEVICE_TABLE(of, db_match);

static struct platform_driver db_driver = {
	.driver = {
		.name = "dct_db",
		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
		.of_match_table = db_match,
	},
	.probe = db_probe,
	.remove = db_remove,
};
module_platform_driver(db_driver);

MODULE_AUTHOR("Axel Ludszuweit <axel.ludszuweit@dreamchip.de>");
MODULE_DESCRIPTION("DCT Doorbell Driver");
MODULE_LICENSE("GPL");
