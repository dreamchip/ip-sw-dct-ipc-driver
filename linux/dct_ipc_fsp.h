/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#ifndef __ZUKIMO_IPC_FSP_H__
#define __ZUKIMO_IPC_FSP_H__

#include <linux/types.h>

#define IPC_FSP_APU_CPU_STATE 0x00000001
#define IPC_FSP_SYSTEM_RESET 0x00000002
#define IPC_FSP_SYSTEM_POWEROFF 0x00000003

#define IPC_FSP_SPP_GPIO_STATE 0x00000010

#define IPC_FSP_SPP_GPIO_CONFIGURE 0x00000011
// #define IPC_FSP_SPP_GPIO_GET_RAW			 0x00000012
#define IPC_FSP_SPP_GPIO_SET_MASKED_RAW 0x00000013
#define IPC_FSP_SPP_GPIO_SET_BITS_RAW 0x00000014
#define IPC_FSP_SPP_GPIO_CLR_BITS_RAW 0x00000015
#define IPC_FSP_SPP_GPIO_TOGGLE_BITS 0x00000016
#define IPC_FSP_SPP_GPIO_GET_RAW 0x00000017
#define IPC_FSP_SPP_GPIO_GET_CFG 0x00000018

#define IPC_FSP_CLK_GET_STATE 0x00000022
#define IPC_FSP_CLK_SET_STATE 0x00000023
#define IPC_FSP_CLK_ROUND_RATE 0x00000024

#define IPC_FSP_VERSION 0x00000030

struct ipc_fsp_apu_cpu_state {
	struct ipc_header hdr;
	uint8_t cpu;
	uint8_t enable;
};

struct ipc_fsp_system_reset {
	struct ipc_header hdr;
};

struct ipc_fsp_system_poweroff {
	struct ipc_header hdr;
};

struct ipc_fsp_spp_gpio_state {
	struct ipc_header hdr;
	uint8_t pin;
	uint8_t enable;
};

struct ipc_fsp_spp_gpio_configure {
	struct ipc_header hdr;
	uint8_t pin;
	uint32_t flags;
};

struct ipc_fsp_spp_gpio_set_masked_raw {
	struct ipc_header hdr;
	uint32_t mask;
	uint32_t value;
};

struct ipc_fsp_spp_gpio_set_bits_raw {
	struct ipc_header hdr;
	uint32_t pins;
};

struct ipc_fsp_spp_gpio_clr_bits_raw {
	struct ipc_header hdr;
	uint32_t pins;
};

struct ipc_fsp_spp_gpio_toggle_bits {
	struct ipc_header hdr;
	uint32_t pins;
};

struct ipc_fsp_spp_gpio_get_raw {
	struct ipc_header hdr;
	union {
		uint32_t rx_value;
	};
};

struct ipc_fsp_spp_gpio_get_cfg {
	struct ipc_header hdr;
	union {
		uint8_t tx_pin;
		uint32_t rx_flags;
	};
};

struct ipc_fsp_clk_state {
	struct ipc_header hdr;
	uint32_t rate_khz;
	uint8_t clk;
	uint8_t on;
};

/* possible values for round_mode: */
#define ipc_fsp_clk_round_nearest 0
#define ipc_fsp_clk_round_up 1
#define ipc_fsp_clk_round_down 2
#define ipc_fsp_clk_round_strict 3
struct ipc_fsp_clk_round_rate {
	struct ipc_header hdr;
	uint32_t rate_khz;
	uint8_t clk;
	uint8_t round_mode;
};

struct ipc_fsp_version {
	struct ipc_header hdr;
	uint32_t alc_version[3];
};

/* ----------------
 * Helper functions for client modules:
 */
struct fsp_version_info {
	uint32_t release_version;
	uint32_t sub_version;
	uint32_t platform_info;
	char *name;
};

const struct fsp_version_info *dct_ipc_fsp_get_version_info(struct platform_device *pdev);
#define FSP_MAJOR_VERSION(vi) ((vi)->release_version >> 16u)
#define FSP_MINOR_VERSION(vi) ((vi)->release_version & 0xffffu)
#define FSP_VARIANT_VERSION(vi) ((vi)->sub_version >> 24u)
#define FSP_PATCH_VERSION(vi) (((vi)->sub_version >> 16u) & 0xffu)
#define FSP_BUILD_VERSION(vi) ((vi)->sub_version & 0xffffu)
#define FSP_BOOT_VERSION(vi) ((vi)->platform_info >> 16u)
#define FSP_BUILD_TYPE(vi) (((vi)->platform_info >> 8u) & 0xffu)
#define FSP_PLATFORM_TYPE(vi) ((vi)->platform_info & 0xffu)
#define FSP_VERSION_NAME(vi) ((vi)->name)

#endif // __ZUKIMO_IPC_FSP_H__
