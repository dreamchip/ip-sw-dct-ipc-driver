/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "dct_ipc.h"
#include "dct_ipc_fsp.h"

static struct fsp_version_info fsp_version_info;

static const char *get_alc_build_type_str(uint32_t built_type_id)
{
	switch (built_type_id) {
		case 1:
			return "Rel";
		case 2:
			return "Dbg";
		case 3:
			return "Cov";
		default:
			break;
	}

	return "Unknown";
}

static const char *get_alc_platform_str(uint8_t platform_type_id)
{
	switch (platform_type_id) {
		case 1:
			return "Eval";
		case 2:
			return "HTG";
		case 3:
			return "HAPS";
		case 4:
			return "ROM";
		case 5:
			return "UVM";
		case 6:
			return "ZUK";
		default:
			break;
	}

	return "Unknown";
}

static const char *get_alc_boot_stage_str(uint8_t stage_id)
{
	switch (stage_id) {
		case 0x30:
			return "SafetyOS";
		default:
			break;
	}

	return "Unknown";
}

static int ipc_fsp_update_peer_version_info(struct platform_device *pdev, struct fsp_version_info *version_info)
{
	struct ipc_fsp_version rx_msg;
	struct ipc_fsp_version tx_msg;
	uint32_t max_rx_data_len;
	int length;
	int rc = 0;

	memset(&rx_msg, 0, sizeof(rx_msg));
	memset(&tx_msg, 0, sizeof(tx_msg));
	tx_msg.hdr.cmd = IPC_FSP_VERSION;
	tx_msg.hdr.len = sizeof(struct  ipc_fsp_version) - sizeof(struct ipc_header);
	max_rx_data_len = sizeof(struct  ipc_fsp_version) - sizeof(struct ipc_header);
	if ((rc = dct_ipc_send_receive(pdev, IPC_FSP, 0,
					(struct ipc_header *)&tx_msg,
					(struct ipc_header *)&rx_msg,
					max_rx_data_len)) != 0) {
		pr_err("Reading Alcatraz version failed\n");
		return -EINVAL;
	}

	version_info->release_version = rx_msg.alc_version[0];
	version_info->sub_version = rx_msg.alc_version[1];
	version_info->platform_info = rx_msg.alc_version[2];

	/* get length first, so we know what to allocate */
	if ((length = snprintf(NULL, 0, "%s-%s-%s V%u.%u.%u.%u.%u",
			get_alc_boot_stage_str(FSP_BOOT_VERSION(version_info)),
			get_alc_build_type_str(FSP_BUILD_TYPE(version_info)),
			get_alc_platform_str(FSP_PLATFORM_TYPE(version_info)),
			FSP_MAJOR_VERSION(version_info),
			FSP_MINOR_VERSION(version_info),
			FSP_VARIANT_VERSION(version_info),
			FSP_PATCH_VERSION(version_info),
			FSP_BUILD_VERSION(version_info))) <= 0) {
		return -EINVAL;
	}

	if (!(version_info->name = kzalloc(length + 1, GFP_KERNEL))) {
		pr_err("kzalloc failed!\n");
		return -ENOMEM;
	}

	snprintf(version_info->name, length + 1, "%s-%s-%s V%u.%u.%u.%u.%u",
			get_alc_boot_stage_str(FSP_BOOT_VERSION(version_info)),
			get_alc_build_type_str(FSP_BUILD_TYPE(version_info)),
			get_alc_platform_str(FSP_PLATFORM_TYPE(version_info)),
			FSP_MAJOR_VERSION(version_info),
			FSP_MINOR_VERSION(version_info),
			FSP_VARIANT_VERSION(version_info),
			FSP_PATCH_VERSION(version_info),
			FSP_BUILD_VERSION(version_info));

	pr_info("Detected Alcatraz version: %s\nraw: %08x %08x %08x\n",
			FSP_VERSION_NAME(version_info),
			version_info->platform_info, version_info->sub_version, version_info->platform_info);

	return 0;
}

const struct fsp_version_info *dct_ipc_fsp_get_version_info(struct platform_device *pdev)
{
	static bool version_info_cached = false;

	if (!version_info_cached) {
		if (ipc_fsp_update_peer_version_info(pdev, &fsp_version_info)) {
			return NULL;
		}

		version_info_cached = true;
	}

	return &fsp_version_info;
}
EXPORT_SYMBOL(dct_ipc_fsp_get_version_info);
